# auth-code-flow example

This example demonstrates the usage of OAuth2 authorization code flow:

https://auth0.com/docs/get-started/authentication-and-authorization-flow/authorization-code-flow

in universis api server environment.

## Usage

1. Clone project at `modules/auth-code-flow-example`  under universis-api root dir.

        git clone https://gitlab.com/kbarbounakis/auth-code-flow-example.git modules/auth-code-flow-example

and install depencencies:

        cd modules/auth-code-flow-example && npm ci

2. Include `modules/auth-code-flow-example` in `.module-aliases.json` as `@universis/auth-code-flow-example`:

        {
            ...
            "@universis/auth-code-flow-example": "modules/auth-code-flow-example/src/index"
        }

3. Update application configuration e.g. `app.development.json` to include `AuthCodeFlowService` service:

        {
            "services": [
                ...
                {
                    "serviceType": "@universis/auth-code-flow-example#AuthCodeFlowService"
                }
            ]
        }

4. Update application configuration to include authorization code flow options under `settings/universis/auth-code-flow-example`:

        "settings": {
            ...
            "universis": {
                "auth-code-flow-example": {
                    "auth": {
                        "authorizationURL": "https://users.universis.io/authorize",
                        "logoutURL": "https://users.universis.io/logout?continue=http://localhost:5001/services/auth-code-flow-example/logout",
                        "tokenURL": "https://users.universis.io/access_token",
                        "scope": "profile",
                        "clientID": "1234567890123",
                        "clientSecret": "secret",
                        "callbackURL": "http://localhost:5001/services/auth-code-flow-example/callback"
                    }
                }
            }
        }

Finally, start api server `npm run serve` and navigate to http://localhost:5001/services/auth-code-flow-example/user

