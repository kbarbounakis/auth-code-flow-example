import { ApplicationService } from "@themost/common";
import { authCodeFlowRouter } from "./authCodeFlowRouter";

/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
function insertRouterBefore(parent, before, insert) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}


export class AuthCodeFlowService extends ApplicationService {
    constructor(app) {
        super(app);
        // extend universis api scope access configuration
        app.container.subscribe((container) => {
            if (container) {
                // get container router
                const router = container._router;
                // find after position
                const before = router.stack.find((item) => {
                    return item.name === 'dataContextMiddleware';
                });
                // use ediplomas router
                container.use('/services/auth-code-flow-example/', authCodeFlowRouter(app));
                if (before == null) {
                    // do nothing
                    return;
                }
                // get last router
                const insert = router.stack[router.stack.length - 1];
                // insert (re-index) router
                insertRouterBefore(router, before, insert);
            }
        });
    }

}