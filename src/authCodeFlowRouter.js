import { HttpError, HttpUnauthorizedError } from '@themost/common';
import {Passport} from 'passport';
import OAuth2Strategy from 'passport-oauth2';
import rateLimit from 'express-rate-limit';
import { Router } from 'express';
import cookieSession from 'cookie-session';
import path from 'path';

class InvalidOrExpiredTokenError extends HttpUnauthorizedError {
    constructor(msg) {
        super(msg || 'invalid/expired token');
        this.status = 401;
        this.code = '1040';
    }
}

/**
 * 
 * @param {import('@themost/express').ExpressDataApplication} app 
 */
function authCodeFlowRouter(app) {

    const router = Router();

    const passport = new Passport();

    /**
     * @type {{authorizationURL:string,tokenURL:string,clientID:string,clientSecret:string,callbackURL:string}}
     */
    const authStategyOptions = app.getConfiguration().getSourceAt('settings/universis/auth-code-flow-example/auth');
    if (authStategyOptions == null) {
        throw new Error('AuthCodeFlowService configuration may not be null');
    }

    passport.use(new OAuth2Strategy(Object.assign(authStategyOptions, {
        passReqToCallback: true
    }), function(req, accessToken, refreshToken, profile, done) {
            /**
             * Gets OAuth2 client services
             * @type {*}
             */
            const client = req.context.getApplication().getStrategy(function OAuth2ClientService() {});
            const translateService = req.context.getApplication().getService(function TranslateService() {});
            // if client cannot be found
            if (typeof client === 'undefined') {
                // throw configuration error
                return done(new Error('Invalid application configuration. OAuth2 client service cannot be found.'))
            }
            if (accessToken == null) {
                // throw 499 Token Required error
                return done(new HttpError(499, 'A token is required to fulfill the request.', 'Token Required'));
            }
            // get token info
            client.getTokenInfo(req.context, accessToken).then(info => {
                if (info == null) {
                    // the specified token cannot be found - 498 invalid token with specific code
                    return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
                }
                // if the given token is not active throw token expired - 498 invalid token with specific code
                if (!info.active) {
                    return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
                }
                return done(null, {
                    "name": info.username,
                    "authenticationType":'AccessToken',
                    "authenticationToken": accessToken,
                    "authenticationScope": info.scope
                });
            }).catch(err => {
                const statusCode = (err && err.statusCode) || 500;
                if (statusCode === 404) {
                    // revert 404 not found returned by auth server to 498 invalid token
                    return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
                }
                // otherwise continue with error
                return done(err);
            });
        }
    ));

    passport.serializeUser(function(user, done) {
        return done(null, user);
    });
    
    passport.deserializeUser(function(user, done) {
        return done(null, user);
    });

    // configure rate limit
    const rateLimitOptions = Object.assign({
        windowMs: 5 * 60 * 1000, // 5 minutes
        max: 20, // 20 requests
        standardHeaders: true,
        legacyHeaders: false,
        keyGenerator: (req) => {
            return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || (req.connection ? req.connection.remoteAddress : req.socket.remoteAddress);
        }
    }, app.getConfiguration().getSourceAt('settings/universis/rateLimit'));
    /**
     * @type {RequestHandler}
     */
    const rateLimitHandler = rateLimit(rateLimitOptions);

    const secret = app.getConfiguration().getSourceAt('settings/crypto/key');

    // use client-side session
    // https://github.com/expressjs/cookie-session#cookie-session
    const sessionCookieOptions = { 
        keys: [
            secret
        ],
        name: 'auth-code-flow-example.sid'
     };
    router.use(cookieSession(sessionCookieOptions));

    // initialize passport
    router.use(passport.initialize());
    router.use(passport.session());
    
    // create context middleware
    router.use((req, _res, next) => {
        // create router context
        const newContext = app.createContext();
        /**
         * try to find if request has already a data context
         * @type {ExpressDataContext|*}
         */
        const interactiveContext = req.context;
        // finalize already assigned context
        if (interactiveContext) {
            if (typeof interactiveContext.finalize === 'function') {
                // finalize interactive context
                return interactiveContext.finalize(() => {
                    // and assign new context
                    Object.defineProperty(req, 'context', {
                        enumerable: false,
                        configurable: true,
                        get: () => {
                            return newContext
                        }
                    });
                    // exit handler
                    return next();
                });
            }
        }
        // otherwise assign context
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
                return newContext
            }
        });
        // and exit handler
        return next();
    });

    // set locale middleware
    router.use((req, _res, next) => {
        // set context locale from request
        req.context.locale  = req.locale;
        // set translate
        const translateService = req.context.application.getStrategy(function TranslateService() {});
        req.context.translate = function() {
            if (translateService == null) {
                return arguments[0];
            }
            return translateService.translate.apply(translateService, Array.from(arguments));
        };
        return next();
    });

    router.use((req, _res, next) => {
        req.context.user = req.session.user;
        return next();
    });

    // use this handler to finalize router context
    // important note: context finalization is very important in order
    // to close and finalize database connections, cache connections etc.
    router.use((req, _res, next) => {
        req.on('end', () => {
            //on end
            if (req.context) {
                //finalize data context
                return req.context.finalize( () => {
                    //
                });
            }
        });
        return next();
    });

    // handle rate limit
    router.use(rateLimitHandler);

    // login
    router.get('/login', passport.authenticate('oauth2', {
        session: true,
        scope: [
            'profile'
        ]
    }));

    // logout
    router.get('/logout', (req, res, next) => {
        req.session = null;
        return res.status(200).render(path.resolve(__dirname, 'views/logout'));
    });

    router.get('/user', (req, res, next) => {
        if (req.context.user == null) {
            return res.redirect('login');
        }
        return next();
    }, (req, res, next) => {
        return req.context.model('User').where('name').equal(req.context.user.name).getItem().then((user) => {
            return res.status(200).render(path.resolve(__dirname, 'views/index'), {
                model: {
                    logoutURL: authStategyOptions.logoutURL,
                    user: user
                },
                html: {
                    context: req.context
                }
            });
        }).catch((err) => {
            return next(err);
        });
    });

    router.get('/callback', passport.authenticate('oauth2', { 
        failureRedirect: 'login',
        session: true,
        scope: [
            'profile'
        ]
    }), (req, res, next) => {
        req.context.user = req.session.user = req.user;
        return res.redirect('user');
    });

    return router;

}

export {
    authCodeFlowRouter
}